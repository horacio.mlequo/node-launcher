apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "thor-gateway.fullname" . }}
data:
  external-ip.sh: |
    #!/bin/bash
    # ./external-ip.sh <host_network> <service_name> <config_map>
    #
    # Script to extract external ip from a service:
    # If host network returns public ip of the node
    # If LoadBalancer returns external IP either directly or from hostname
    # If ClusterIP return service IP
    # If NodePort returns node IP

    apk add bind-tools

    HOST_NETWORK=$1
    SERVICE=$2
    CONFIGMAP=$3

    if [ "$HOST_NETWORK" = "true" ]; then
      external_ip=$(curl -s http://whatismyip.akamai.com)
    else
      type=$(kubectl get svc $SERVICE -o jsonpath='{.spec.type}')
      external_ip=""

      if [ "$type" = "ClusterIP" ]; then
        external_ip=$(kubectl get svc $SERVICE -o jsonpath='{.spec.clusterIP}')
      elif [ "$type" = "NodePort" ]; then
        external_ip=$(kubectl get nodes --selector=kubernetes.io/role!=master -o jsonpath='{.items[0].status.addresses[?(@.type=="ExternalIP")].address}')
      elif [ "$type" = "LoadBalancer" ]; then
        # Hack TODO remove when this is fixed here https://github.com/kubernetes/kubernetes/issues/82595
        kubectl annotate svc $SERVICE service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled=false --overwrite
        sleep 5
        kubectl annotate svc $SERVICE service.beta.kubernetes.io/aws-load-balancer-cross-zone-load-balancing-enabled=true --overwrite

        while [ -z $external_ip ]; do
          echo "Waiting for load balancer external endpoint..."
          external_ip=$(kubectl get svc $SERVICE -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
          if [ -z $external_ip ]; then
            hostname=$(kubectl get svc $SERVICE -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')
            [ ! -z $hostname ] && external_ip=$(dig +short $hostname | sort | head -1)
          fi
          [ -z $external_ip ] && sleep 10
        done
      fi
    fi

    kubectl create configmap $CONFIGMAP --from-literal=externalIP=$external_ip --dry-run=client -o yaml | kubectl apply -f -

  haproxy.cfg: |
    global
        maxconn 2000

    defaults
        mode tcp
        timeout connect 5000ms
        timeout client 50000ms
        timeout server 50000ms

    listen health-check
      bind :80
      mode http
      monitor-uri /healthz
      option      dontlognull

    {{- if .Values.validator }}
    listen bifrost-p2p
        bind *:{{ .Values.service.port.bifrost.p2p }}
        server bifrost_service bifrost:{{ .Values.service.port.bifrost.p2p }}

    listen bifrost-api
        bind *:{{ .Values.service.port.bifrost.api }}
        server bifrost_service bifrost:{{ .Values.service.port.bifrost.api }}
    {{- end }}

    listen thor-daemon-rpc
        bind *:{{ include "thor-gateway.rpc" . }}
        server thor_daemon_service thor-daemon:{{ include "thor-gateway.rpc" . }}

    listen thor-daemon-p2p
        bind *:{{ include "thor-gateway.p2p" . }}
        server thor_daemon_service thor-daemon:{{ include "thor-gateway.p2p" . }}

    listen midgard
        bind *:{{ .Values.service.port.midgard }}
        server midgard_service midgard:{{ .Values.service.port.midgard }}

    listen api
        bind *:{{ .Values.service.port.api }}
        server thor_api_service thor-api:{{ .Values.service.port.api }}

  nginx.conf: |
    user  nginx;
    worker_processes  auto;

    error_log  /var/log/nginx/error.log warn;
    pid        /var/run/nginx.pid;

    events {
      worker_connections  1024;
    }

    http {
      include       /etc/nginx/mime.types;
      default_type  application/octet-stream;
      log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                        '$status $body_bytes_sent "$http_referer" '
                        '"$http_user_agent" "$http_x_forwarded_for"';

      access_log  /var/log/nginx/access.log  main;

      sendfile        on;
      keepalive_timeout  65;

      proxy_cache_path  /var/cache/nginx/thornode levels=1:2 keys_zone=thornode:10m;

      server {
        listen 80;
        location /healthz {
          access_log off;
          return 200 "healthy\n";
        }
      }

      server {
        listen {{ .Values.service.port.midgard }};
        location / {
          add_header Access-Control-Allow-Credentials true;
          add_header Access-Control-Allow-Methods 'DELETE,GET,OPTIONS,POST,PUT';
          add_header Access-Control-Allow-Headers 'Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Requested-With,X-Token-Auth,X-Mx-ReqToken,X-Requested-With';

          add_header Cache-Control "no-cache, must-revalidate, max-age=0";
          add_header X-Cache-Status $upstream_cache_status;
          proxy_cache thornode;
          proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;
          proxy_cache_lock on;
          proxy_cache_background_update on;
          proxy_cache_valid any 5s;
          proxy_ignore_headers X-Accel-Expires Expires Cache-Control;
          proxy_pass http://midgard:{{ .Values.service.port.midgard }};
        }
      }
    }


    stream {
      {{- if .Values.validator }}
      server {
        listen {{ .Values.service.port.bifrost.p2p }};
        proxy_pass bifrost:{{ .Values.service.port.bifrost.p2p }};
      }

      server {
        listen {{ .Values.service.port.bifrost.api }};
        proxy_pass bifrost:{{ .Values.service.port.bifrost.api }};
      }

      {{- end }}
      server {
        listen {{ .Values.service.port.api }};
        proxy_pass thor-api:{{ .Values.service.port.api }};
      }

      server {
        listen {{ include "thor-gateway.p2p" . }};
        proxy_pass thor-daemon:{{ include "thor-gateway.p2p" . }};
      }

      server {
        listen {{ include "thor-gateway.rpc" . }};
        proxy_pass thor-daemon:{{ include "thor-gateway.rpc" . }};
      }
    }
